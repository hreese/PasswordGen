#!/usr/bin/env python3

import fileinput

def myTranslate(word):
    return word.replace("Ä", "Ae").replace("Ö", "Oe").replace("Ü", "Ue").replace("ä", "ae").replace("ö", "oe").replace("ü", "ue").replace("ß", "ss")

worddict = {}

for line in fileinput.input("DeReKo-2014-II-MainArchive-STT.100000.freq"):
    parts = line.split("\t")
    for w in (myTranslate(parts[0]),  myTranslate(parts[1])):
        if len(w)>3 and len(w) < 7:
            worddict[w] = float(parts[3].strip())

print(sorted(worddict.items(), key=worddict.__getitem__))
