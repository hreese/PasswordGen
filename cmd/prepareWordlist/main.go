package main

import (
	"bufio"
	"flag"
	"log"
	"os"
	"unicode"

	"strings"

	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
	"golang.org/x/text/runes"
	"github.com/texttheater/golang-levenshtein/levenshtein"
	"fmt"
	"sort"
)

const (
	MINLEN        = 4
	MAXLEN        = 6
	MAXUPPERRATIO = 0.3
)

var (
	germanReplacer = strings.NewReplacer("Ä", "Ae", "Ö", "Oe", "Ü", "Ue", "ä", "ae", "ö", "oe", "ü", "ue", "ß", "ss")
)

func main() {
	var (
		wordmap = make(map[string]bool)
	)

	flag.Parse()
	if flag.NArg() < 1 {
		log.Fatal("No input files specified")
	}

	// read all input files
	for _, infilename := range flag.Args() {
		infile, err := os.Open(infilename)
		if err != nil {
			log.Fatal(err)
		}
		defer infile.Close()

		scanner := bufio.NewScanner(infile)
		for scanner.Scan() {
			// trim space
			word := strings.TrimSpace(scanner.Text())
			// remove umlauts
			//word = germanReplacer.Replace(word)
			// skip german special chars
			// TODO: make more generic, only accept certain ASCII chars
			if strings.ContainsAny(word, "ÄÖÜäöüß'") {
				continue
			}
			// remove accents
			word = CleanString(word)
			// filter unfit words based on length and upper-/lowercase distribution
			if WordFilter(word) {
				wordmap[word] = true
			}
		}
	}

	wordlist := make([]string, 0, len(wordmap))
	for w := range wordmap {
		wordlist = append(wordlist, w)
	}

	type WordDistance struct {
		Word string
		Distance int
	}

	var ldlist []WordDistance
	// calculate every word's Levenshtein distance to all other words
	onlythesewords := wordlist[:]
	numwords := len(onlythesewords)
	for idx, word := range onlythesewords {
		ldistance := 0
		if idx % (numwords/100) == 0 {
			fmt.Fprintf(os.Stderr, "%d%% ", int(float64(idx)/float64(numwords)*100))
		}
		for _, compareword := range onlythesewords {
			ldistance += levenshtein.DistanceForStrings([]rune(word), []rune(compareword), levenshtein.DefaultOptions)
		}
		ldlist = append(ldlist, WordDistance{ word, ldistance})
	}
	fmt.Fprintln(os.Stderr, "100%")

	// sort by Levenshtein distance
	sort.Slice(ldlist, func(i, j int) bool {
		return ldlist[i].Distance > ldlist[j].Distance
	})

	for _, word := range ldlist {
		fmt.Println(word.Word)
	}
}

func WordFilter(word string) bool {
	// check length
	if len(word) >= MINLEN && len(word) <= MAXLEN  {
		// check uppercase to test ratio
		numUpper := 0
		for _, r := range []rune(word) {
			if unicode.IsUpper(r) {
				numUpper++
			}
		}
		if float64(numUpper)/float64(len(word)) > MAXUPPERRATIO {
			return false
		}
			return true
	}
	return false
}

func CleanString(in string) string {
	// remove accents and spacing
	b := make([]byte, len(in))
	t := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	_, _, e := t.Transform(b, []byte(in), true)
	if e != nil {
		panic(e)
	}
	for i:=len(b)-1; i >= 0; i-- {
		if b[i] == 0 {
			b = b[:i]
		}
	}
	return string(b)
}